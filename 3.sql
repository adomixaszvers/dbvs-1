﻿-- Kiekvienam skaitytojui (vardas, pavardė) ir datai, kai jis turi grąžinti bent vieną egzempliorių, grąžintinų egzempliorių skaičius.
--with
--  laikinas as (select
--    skaitytojas.nr,
--    skaitytojas.vardas,
--    skaitytojas.pavarde,
--    egzempliorius.grazinti
--  from
--    stud.skaitytojas,
--    stud.egzempliorius
--  where
--    skaitytojas.nr = egzempliorius.skaitytojas
--)
select
  skaitytojas.vardas,
  skaitytojas.pavarde,
  egzempliorius.grazinti,
  count(egzempliorius.grazinti) as grazintina_knygu
--  (select
--    count(grazinti)
--  from
--    stud.egzempliorius
--  where
--    grazinti <= laikinas.grazinti and laikinas.nr = skaitytojas) as grazintina_knygu
from
  stud.skaitytojas,
  stud.egzempliorius
where
  skaitytojas.nr = egzempliorius.skaitytojas
group by
  skaitytojas.vardas, skaitytojas.pavarde, egzempliorius.grazinti
order by
  egzempliorius.grazinti;
