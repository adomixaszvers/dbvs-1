﻿-- Numeriai ir ISBN visų egzempliorių, kurių numeris yra nelyginis skaičius.
SELECT 
  nr,
  isbn
FROM
  stud.egzempliorius
WHERE
  nr%2=1
ORDER BY
  asc nr;