﻿-- Numeriai, vardai ir pavardės skaitytojų, konkrečią dieną paėmusių bent po vieną egzempliorių.
SELECT 
  skaitytojas.nr, 
  skaitytojas.vardas, 
  skaitytojas.pavarde 
FROM 
  stud.skaitytojas,
  stud.egzempliorius
WHERE 
  skaitytojas.nr = egzempliorius.skaitytojas
and
  egzempliorius.paimta=date '2004-09-02';
