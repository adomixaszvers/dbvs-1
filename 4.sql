﻿-- Vardai ir pavardės skaitytojų, kurių yra paėmę mažiau egzempliorių nei vidutiniškai.
create temp table
  paeme_knygu as (select
    skaitytojas.vardas,
    skaitytojas.pavarde,
    count(egzempliorius.skaitytojas) as knygu
  from
    stud.skaitytojas, stud.egzempliorius
  where
    skaitytojas.nr=egzempliorius.skaitytojas
  group by skaitytojas.nr
  );
create temp table
  vidutiniskai as (select
    avg(cast(knygu as integer)) as knygu
  from paeme_knygu);
select
  paeme_knygu.vardas,
  paeme_knygu.pavarde
from
  paeme_knygu,
  vidutiniskai
where
  paeme_knygu.knygu < (select knygu from vidutiniskai);
drop table paeme_knygu, vidutiniskai;
