﻿-- Kiekvienam stulpelio tipui bendras visų lentelių visų tokio tipo stulpelių skaičius.
with
  stulpeliu_tipai as (
  select distinct
    data_type
  from information_schema.columns
)
select
  columns.data_type,
  count(column_name)
from
  information_schema.columns,
  stulpeliu_tipai
where
  columns.data_type=stulpeliu_tipai.data_type
group by
  columns.data_type;
